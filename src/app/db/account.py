from config import Config
from sqlalchemy import Column, DateTime, Integer, String, Text, Sequence, Boolean, UniqueConstraint
from db.abstract import DbEntity
from sqlalchemy.ext.declarative import declarative_base
from pblib.amqp import client
import copy
import datetime
import json
import requests
import os
from time import sleep

Base = declarative_base()
config = Config()


class Account(DbEntity, Base):
    __tablename__ = 'accounts'
    __table_args__ = (UniqueConstraint('login_id', name='login_id'),
                      {'mysql_engine': 'InnoDB', 'mysql_charset': 'utf8mb4', 'mysql_collate': 'utf8mb4_general_ci'})

    id = Column(Integer, primary_key=True, autoincrement=True)
    login = Column(String(255))
    login_id = Column(String(255))
    data = Column(Text)

    def __init__(self):
        super().__init__()
        Base.metadata.create_all(Config.db_engine)

    def __repr__(self):
        return "<Account(id='%s', login='%s')>\r\n%s" % (
            self.id, self.login, self.data)

    def save(self):
        self.fire()

    # def commit(self):
    #     raise Exception("For events, usage of commit and add functions are forbidden. Either use save() oder fire()")

    def add(self):
        raise Exception("For events, usage of commit and add functions are forbidden. Either use save() oder fire()")

    def rollback(self):
        raise Exception("For events, usage of rollback is forbidden.")

    def fire(self):
        message = copy.deepcopy(self.__dict__)
        if "_sa_instance_state" in message:
            del message["_sa_instance_state"]

        self.session.add(self)
        self.session.commit()

    def getData(self, login=None, apikey=None):
        login_id = None
        if login is None:
            loginData = self.getLoginDataByApikey(apikey)
            login = loginData.get("login")
            login_id = loginData.get("id")

        account = Config.db_session.query(Account).filter(Account.login == login).first()

        # print("DB_ACCOUNT", account)

        if account is not None and account.data:
            data = account.data
        else:
            account = Account()
            account.login = login
            account.login_id = login_id
            account.data = json.dumps({"event_data_filter": {}, "reminder_email_configuration": {
                "recipients": [],
                "header": "",
                "footer": ""
            }})
            try:
                account.fire()
            except Exception as e:
                print("EXCEPTION e", e)

            data = account.data

        if account.login_id is None and apikey is not None:
            loginData = self.getLoginDataByApikey(apikey)
            login = loginData.get("login")
            login_id = loginData.get("id")
            account.login_id = login_id
            account.commit()

        # print("DATA", data)
        return data

    def getLoginByApikey(self, apikey):
        response_json = self.getLoginDataByApikey(apikey)
        return response_json.get("login", "")

    def getLoginDataByApikey(self, apikey):
        gateway_url = os.environ.get("APIGATEWAY_INTERNAL_BASEURL")
        url = gateway_url + "/get_login_by_apikey?apikey=" + apikey
        querystring = {"apikey": apikey}

        headers = {"apikey": apikey}

        response = requests.request("GET", url, headers=headers, params=querystring)
        return response.json()

    def getConsumerByLoginId(self, login_id):
        gateway_url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL")
        url = gateway_url + f"/basic-auths/{login_id}/consumer"
        print("LOGIN BY LOGINID KEY URL", url)

        response = requests.request("GET", url)
        return response.json()


from sqlalchemy.ext.declarative import DeclarativeMeta


class AlchemyEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            for field in [x for x in dir(obj) if not x.startswith('_') and x != 'metadata']:
                data = obj.__getattribute__(field)
                try:
                    json.dumps(data)  # this will fail on non-encodable values, like other classes
                    fields[field] = data
                except TypeError:
                    fields[field] = None
            # a json-encodable dict
            return fields

        return json.JSONEncoder.default(self, obj)
