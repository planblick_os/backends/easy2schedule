import sys
sys.path.append("/src/app")
from config import Config

import datetime
import pytz
import json
import uuid
from pbglobal.commands.sendAppointmentReminder import sendAppointmentReminder

# Compensate for processing-time in seconds. 10 Means, reminder will be triggered 10 seconds earlier than set
compensation_value = 10

sql = "SELECT * FROM event_reminders as er inner join events AS e on e.event_id = er.event_id where processed_time is null"
results = Config.db_session.execute(sql).fetchall()
Config.db_session.commit()
current_timezone = pytz.timezone("Europe/Berlin")

for row in results:
    current_time = datetime.datetime.now(pytz.timezone('Europe/Berlin'))
    command_creation_time = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    localized_starttime = current_timezone.localize(row.starttime)
    seconds_until_event = localized_starttime.timestamp() - current_time.timestamp()

    # Only do something if event hasn't passed already
    if seconds_until_event >= -600:
        reminder_definition = json.loads(row.definition)

        if reminder_definition.get("seconds_in_advance") + compensation_value >= seconds_until_event:
            if reminder_definition.get("reminder_type") == "gui":
                sent_command = sendAppointmentReminder(reminder_id=row.reminder_id, owner=row.owner, creator=row.creator, correlation_id=str(uuid.uuid4()), create_time=command_creation_time)
                sent_command.publish()
                sql = f"UPDATE event_reminders SET processed_time = now() where processed_time is null and reminder_id = :reminder_id"
                results = Config.db_session.execute(sql, {"reminder_id": row.reminder_id})
                Config.db_session.commit()
            elif reminder_definition.get("reminder_type") == "email":
                sent_command = sendAppointmentReminder(reminder_id=row.reminder_id, owner=row.owner, creator=row.creator, correlation_id=str(uuid.uuid4()), create_time=command_creation_time)
                sent_command.publish()
                sql = f"UPDATE event_reminders SET processed_time = now() where processed_time is null and reminder_id = :reminder_id"
                results = Config.db_session.execute(sql, {"reminder_id": row.reminder_id})
                Config.db_session.commit()


