from main import app
from planblick.httpserver import Server
from pbglobal.pblib.amqp import client
from models.message_handler import message_handler

from autorun import Autorun

if __name__ == '__main__':
    Autorun().post_deployment()

    name = "calendar-backend"
    queue = "calendar-backend"
    bindings = {}
    bindings['message_handler'] = ['createNewAppointment',
                                   'changeAppointment', 'deleteAppointment', "createCalendarRessource",
                                   "newAppointmentCreated", "appointmentChanged", "appointmentDeleted",
                                   "calendarRessourceCreated", "deleteCalendarRessource", "calendarRessourceDeleted",
                                   "updateCalendarColumns", "calendarColumnsUpdated", "sendNextWeekReminderEmails",
                                   "nextWeekReminderEmailsSent",
                                   "setEasy2ScheduleExtendedProperties", "easy2scheduleExtendedPropertiesSet",
                                   "setEasy2ScheduleReminderEmailConfiguration",
                                   "easy2scheduleReminderEmailConfigurationSet",
                                   "easy2ScheduleAddDataOwnerToEvent", "easy2scheduleDataOwnerAddedToEvent",
                                   "easy2ScheduleRemoveDataOwnerFromEvent", "easy2scheduleDataOwnerRemovedFromEvent",
                                   "setEasy2ScheduleEventReminder", "easy2ScheduleEventReminderSet",
                                   "sendAppointmentReminder", "appointmentReminderSent",
                                   "updateCalendarRessource", "calendarRessourceUpdated",
                                   "easy2scheduleDeleteReminder", "easy2scheduleReminderDeleted"]
    callback = message_handler.handle_message

    client.addConsumer(name, queue, bindings, callback)

    server = Server()
    server.start(flask_app=app, app_path="/cb", static_dir="/static", static_path="/cb/docs", port=8000)
