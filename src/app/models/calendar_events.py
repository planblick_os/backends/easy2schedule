import json
from db.account import Account
from pblib import mail_send
from datetime import datetime, timedelta
import traceback
import requests
import os
from time import sleep
from config import Config
from db.columns import Columns
from models.api_calls import ApiCalls


class CalendarEvents():

    def __init__(self, db_session):
        self.dbsession = db_session

    def getReminderEmailConfigs(self, login=None):
        if login is None:
            sql = f'SELECT * FROM accounts WHERE login IS NOT NULL'
            self.dbsession.expire_all()
            results = self.dbsession.execute(sql).fetchall()
            self.dbsession.commit()
        else:
            sql = f'SELECT * FROM accounts WHERE login = :login'
            self.dbsession.expire_all()
            results = self.dbsession.execute(sql, {"login": login}).fetchall()
            self.dbsession.commit()

        returnValue = []
        for result in results:
            data = json.loads(result["data"])
            returnValue.append(
                {"login_name": result["login"], "email_config": data.get("reminder_email_configuration", {})})
        return returnValue

    def filter_events_by_personal_settings(self, events, email_config):
        filtered_events = []
        for event in events:

            is_own = False
            is_assigned_to_me = False

            if event.get("creator") == email_config.get("login_name"):
                is_own = True

            for attached_ressource in event.get("attachedRessources"):
                attached_ressource = json.loads(attached_ressource)
                if email_config.get("login_name") == attached_ressource.get("name") or email_config.get(
                        "login_name") == attached_ressource.get("loginId"):
                    is_assigned_to_me = True

            filter_config = email_config.get("email_config", {}).get("event_filter") or {}

            if filter_config.get("other") is True:
                filtered_events.append(event)
                continue

            if filter_config.get("owned") is True and is_own is True:
                filtered_events.append(event)
                continue

            if filter_config.get("assigned") is True and is_assigned_to_me is True:
                filtered_events.append(event)
                continue

        return filtered_events

    def sendNextWeekReminderEmails(self):
        email_configs = self.getReminderEmailConfigs()
        mails_sent_to = []

        days_in_future_for_start = 1
        days_in_future_for_end = 7
        today = datetime.now()  # or .today()
        start_date = (today + timedelta(days=days_in_future_for_start)).replace(hour=0, minute=0, second=0,
                                                                                microsecond=0)
        end_date = (start_date + timedelta(days=days_in_future_for_end)).replace(hour=23, minute=59, second=0,
                                                                                 microsecond=0)

        for email_config in email_configs:
            consumer = Account().getConsumerByLoginId(email_config.get("login_name"))
            consumer_id = consumer.get("custom_id")
            email_config["consumer_id"] = consumer_id
            email_config["consumer_name"] = consumer.get("username")

            try:
                events = self.get_personal_events_by_login_id(
                    consumer_id=consumer_id,
                    login_id=email_config.get("login_name"),
                    start_date=start_date,
                    end_date=end_date
                )

                event_table = ""

                for event in events:
                    event["ressource_name"] = self.resolveRessourceId(event["resourceId"], event["owner"])
                    event["attached_ressource_names"] = [item.get("name") for item in
                                                         [json.loads(res) for res in event["attachedRessources"]]]
                    event["starttime"] = datetime.fromisoformat(event.get('starttime')).strftime("%d.%m.%Y, %H:%M")
                    event["endtime"] = datetime.fromisoformat(event.get('endtime')).strftime("%d.%m.%Y, %H:%M")
                    event_table += f"""
------------------------------------
Start: {event.get('starttime', '')}
Ende: {event.get('endtime', '')}
Titel: {event.get('title', '')}
------------------------------------
"""

                events = self.filter_events_by_personal_settings(events=events, email_config=email_config)
                
                if email_config is not None and email_config.get("email_config", {}).get("recipients") is not None:
                    for mail in email_config.get("email_config", {}).get("recipients"):
                        result = mail_send.send_reminder_mail(mail, event_table, events, email_config)
                        if result is True:
                            mails_sent_to.append(mail)
                        else:
                            # notify us using slack
                            if os.getenv("SLACK_NOTIFICATION_HOOK"):
                                url = os.getenv("SLACK_NOTIFICATION_HOOK")

                                payload = json.dumps({
                                    "text": f"Unable to send Easy2Schedule weekly reminder email to address: {mail} on environment {os.getenv('ENVIRONMENT')}"
                                })
                                headers = {
                                    'Content-Type': 'application/json'
                                }

                                response = requests.request("POST", url, headers=headers, data=payload)
                                # Make sure we do not spam Slack to much
                                sleep(10)

                    print("SENT EMAILS FOR TIMESPAN", start_date, end_date)
            except Exception as e:
                print(e)
                traceback.print_exc()

        return mails_sent_to

    def get_personal_events_by_apikey(self, apikey, start_date, end_date):
        loginData = Account().getLoginDataByApikey(apikey)
        sql = f'SELECT * FROM events WHERE owner=:consumer_id AND  (endtime > :start_date AND starttime BETWEEN :start_date AND :end_date OR starttime < :end_date AND endtime BETWEEN :start_date AND "{end_date}"  OR (starttime < :start_date AND endtime > :end_date)) order by starttime, endtime'
        results = self.dbsession.execute(sql, {"start_date": start_date, "end_date": end_date,
                                               "consumer_id": loginData.get("consumer_id", "false")}).fetchall()

        events = []
        print("RESULTS", loginData )
        for instance in results:
            try:
                event_data = instance.payload
                event_data = json.loads(event_data)
                if event_data.get("creator") == loginData.get("id") or event_data.get("creator") == loginData.get("login"):
                    events.append(event_data)
                else:
                    for attachedRessource in event_data.get("attachedRessources", "[]"):
                        attachedRessource = json.loads(attachedRessource)
                        if attachedRessource.get("loginId") == loginData.get("id") or attachedRessource.get("loginId") == loginData.get("login"):
                            events.append(event_data)
                            break
            except Exception as e:
                print(e)
                traceback.print_exc()

        return events

    def get_personal_events_by_login_id(self, consumer_id, login_id, start_date, end_date):

        sql = f'SELECT * FROM events WHERE owner=:consumer_id AND  (endtime > :start_date AND starttime BETWEEN :start_date AND :end_date OR starttime < :end_date AND endtime BETWEEN :start_date AND "{end_date}"  OR (starttime < :start_date AND endtime > :end_date)) order by starttime'
        results = self.dbsession.execute(sql, {"start_date": start_date, "end_date": end_date,
                                               "consumer_id": consumer_id}).fetchall()

        events = []

        users_group_memberships = ApiCalls().getGroupMembershipsByConsumerAndLogin(consumer_id, login_id)

        for instance in results:
            event_data = instance.payload
            event_data = json.loads(event_data)

            data_owners = json.loads(instance.data_owners)
            matches = [match for match in users_group_memberships if match in data_owners]
            if f"user.{login_id}" in data_owners:
                matches.append(f"user.{login_id}")

            if len(matches) == 0 and len(data_owners) > 0:
                continue

            events.append(event_data)
            # if event_data.get("creator") == login_id:
            #     events.append(event_data)
            # else:
            #     for attachedRessource in event_data.get("attachedRessources","[]"):
            #         attachedRessource = json.loads(attachedRessource)
            #         if attachedRessource.get("loginId") == login_id:
            #             events.append(event_data)
            #             break

        return events

    def resolveRessourceId(self, ressource_id, consumer_id):
        data = Config.db_session.query(Columns).filter_by(owner_id=consumer_id).first()
        Config.db_session.commit()

        if data is None:
            with open('/src/app/ressources/default_calendar_columns.json') as json_file:
                data = json.load(json_file)
        else:
            data = json.loads(data.data)

        for view, settings in data.items():
            for column in settings:
                if ressource_id == column.get("id"):
                    return column.get("title")
