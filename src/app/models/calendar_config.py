import json
from config import Config
from db.columns import Columns


class CalendarConfig:
    def __init__(self, owner_id):
        self.owner_id = owner_id

    def get_columns(self):
        data = Config.db_session.query(Columns).filter_by(owner_id=self.owner_id).first()
        Config.db_session.commit()
        if data is not None:
            print("FOO", data)
            return json.loads(data.data)
        else:
            with open('/src/app/ressources/default_calendar_columns.json') as json_file:
                data = json_file.read()
            return json.loads(data)
