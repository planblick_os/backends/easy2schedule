def filter_dict(filter, dictionary):

    for key, value in filter.items():
        if isinstance(value, dict):
            # get node or create one
            node = dictionary.setdefault(key, {})
            filter_dict(value, node)
        else:
            dictionary[key] = value

    return dictionary