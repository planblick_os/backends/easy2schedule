#!/bin/sh
#_term() {
#  echo "Caught SIGTERM signal!"
#  python /src/app/teardown.py
#}

#_termint() {
#  echo "Caught SIGINT signal!"
#  python /src/app/teardown.py
#}

#trap _term SIGTERM
#trap _termint SIGINT


echo "Starting crond..."
crontab -l

crond -f -l 8 &
python /src/app/events/socketio-worker.py &
python /src/app/server.py

#child=$!
#wait "$child" &
