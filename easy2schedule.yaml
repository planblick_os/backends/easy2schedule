apiVersion: v1
kind: Service
metadata:
  name: easy2schedule
  namespace: planblick
  labels:
    app: easy2schedule
spec:
  ports:
  - port: 8000
    targetPort: 8000
  selector:
    app: easy2schedule
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: easy2schedule
  namespace: planblick
  labels:
    app: easy2schedule
spec:
  replicas: 1
  selector:
    matchLabels:
      app: easy2schedule
  template:
    metadata:
      labels:
        app: easy2schedule
    spec:
      containers:
      - name: easy2schedule
        image: [image]
        env:
        - name: ENVIRONMENT
          valueFrom:
            configMapKeyRef:
              name: environment
              key: ENVIRONMENT
        - name: SLACK_NOTIFICATION_HOOK
          valueFrom:
            configMapKeyRef:
              name: environment
              key: SLACK_NOTIFICATION_HOOK
        - name: APIGATEWAY_ADMIN_INTERNAL_BASEURL
          valueFrom:
            configMapKeyRef:
              name: environment
              key: APIGATEWAY_ADMIN_INTERNAL_BASEURL
        - name: APIGATEWAY_INTERNAL_BASEURL
          valueFrom:
            configMapKeyRef:
              name: environment
              key: APIGATEWAY_INTERNAL_BASEURL
        - name: INTERNAL_ACCOUNTMANAGER_URL
          valueFrom:
            configMapKeyRef:
              name: environment
              key: INTERNAL_ACCOUNTMANAGER_URL
        - name: DB_HOST
          valueFrom:
            configMapKeyRef:
              name: environment
              key: MYSQL_HOST
        - name: DB_PORT
          valueFrom:
            configMapKeyRef:
              name: environment
              key: MYSQL_PORT
        - name: DB_DRIVER
          value: "mysql+pymysql"
        - name: DB_USER
          valueFrom:
            secretKeyRef:
              name: mysql-user-credentials
              key: username
        - name: DB_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysql-user-credentials
              key: password
        - name: DB_NAME
          value: "easy2schedule"
        - name: DB_DEBUG
          value: "no"
        - name: MQ_HOST
          value: "rabbitmq.planblick.svc"
        - name: CERTIFICATE_STORE_BASE_URL
          value: "http://certificate-store.planblick.svc:8000"
        - name: ACCOUNTMANAGER_STORE_BASE_URL
          value: "http://account-manager.planblick.svc:80"
        - name: MQ_USERNAME
          valueFrom:
            secretKeyRef:
              name: rabbitmq-credentials
              key: username
        - name: MQ_PASSWORD
          valueFrom:
            secretKeyRef:
              name: rabbitmq-credentials
              key: password
        - name: MQ_PORT
          value: "5672"
        - name: MQ_VHOST
          value: "/"
        - name: MAIL_CERTIFICATES_KEY_PATH
          valueFrom:
            configMapKeyRef:
              name: environment
              key: MAIL_CERTIFICATES_KEY_PATH
        - name: MAIL_CERTIFICATES_CRT_PATH
          valueFrom:
            configMapKeyRef:
              name: environment
              key: MAIL_CERTIFICATES_CRT_PATH
        - name: SMTP_HOST
          valueFrom:
            configMapKeyRef:
              name: environment
              key: SMTP_HOST
        - name: SMTP_PORT
          valueFrom:
            configMapKeyRef:
              name: environment
              key: SMTP_PORT
        - name: SMTP_AUTH_NEEDED
          valueFrom:
            configMapKeyRef:
              name: environment
              key: SMTP_AUTH_NEEDED
        - name: SMTP_SSL
          valueFrom:
            configMapKeyRef:
              name: environment
              key: SMTP_SSL
        - name: SMTP_USERNAME
          valueFrom:
            secretKeyRef:
              name: smtp-credentials
              key: username
        - name: SMTP_PASSWORD
          valueFrom:
            secretKeyRef:
              name: smtp-credentials
              key: password
        ports:
        - containerPort: 8000
        volumeMounts:
        - name: certs
          mountPath: /src/certs
        livenessProbe:
          httpGet:
            path: /cb/healthz
            port: 8000
          initialDelaySeconds: 20
          periodSeconds: 10
          timeoutSeconds: 60
          successThreshold: 1
          failureThreshold: 12
      volumes:
      - name: certs
        configMap:
          name: certs